﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour
{
    public Bounds bounds;
    public float speed = 0.5f;
	// Use this for initialization
	void Start () {
	
	}

    public void Move(Vector2 movement)
    {
        
        
        if (bounds.Contains(transform.position + (Vector3)movement))
            transform.Translate(movement * speed);
    }

    // Update is called once per frame
	void Update ()
	{
	    GetComponent<Rigidbody>().velocity = Vector3.zero;
	    
	}
}
