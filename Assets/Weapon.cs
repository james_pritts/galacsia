﻿    using System.Security.Cryptography;
    using UnityEngine;
    using System.Collections;

    public class Weapon : MonoBehaviour {
        public float bulletSpeed = 20f;
        private  WeaponInput _weaponInput;
        public string enemyTag;
        public AudioClip fire;
        public float bulletSize=0.1f;
  
        // Use this for initialization
	    void Init ()
	    {
	    //    fire = (AudioClip) Resources.Load("Shoot1");
	   
	   
	     //   audio.clip = fire;
	    }

        public void Start()
        {
            if (GetComponent<AudioSource>())
            {
                Init();
            }
        }

        public void OnCollisionEnter(Collision c)
        {
            if (c.gameObject.tag == "Player"&&c.gameObject.GetComponent<Weapon>()!=null)
            {Debug.Log(c.gameObject);
                c.gameObject.GetComponent<Weapon>().bulletSize += bulletSize;
                Destroy(gameObject);
            }
        }

        public void Shoot(int l)

        {   GetComponent<AudioSource>().Play();
            GameObject bullet = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            bullet.transform.localScale = Vector3.one*bulletSize;
            bullet.transform.position = transform.position + transform.up * (GetComponent<Collider>().bounds.size.z + bullet.GetComponent<Collider>().bounds.size.z);
        
            bullet.transform.Rotate(90f,0f,0f);
            bullet.AddComponent<Rigidbody>();
            bullet.GetComponent<Rigidbody>().useGravity = false;
            bullet.GetComponent<Rigidbody>().velocity = transform.up*bulletSpeed;
            bullet.tag = enemyTag;
            bullet.layer = gameObject.layer;
            Destroy(bullet,1.5f);
        }
      
        // Update is called once per frame
    }
