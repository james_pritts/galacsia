﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControllerInput : MonoBehaviour
{  
    public ShipController controller;
    public bool isGalaga= false;
    public Joystick joystick;
    public bool isMobile = true;
    public Vector2 bounds ;
    public GameManager manager;

	// Use this for initialization
	void Start ()
	{
	    manager = FindObjectOfType<GameManager>();
	    joystick = FindObjectOfType<Joystick>();
	    controller = GetComponent<ShipController>();
      //  FindObjectOfType<Button>().onClick.AddListener(()=>);
	}

    public void FixedUpdate()
    {
        if(isMobile)
            controller.Move(joystick.position);
        if(isGalaga)
    controller.Move(new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical")));
        else
            controller.Move(new Vector2(Input.GetAxis("Horizontal"),0));
  
    }

    public void OnDestroy()
    {manager.RemoveLife();
    }

    // Update is called once per frame
	void Update () {
	
	}
}
