using UnityEngine;
using UnityEngine.UI;

public class WeaponInput :MonoBehaviour
{
    private Weapon _weapon;
    public Button button;
    public void Start()
    {
        _weapon = GetComponent<Weapon>();
       // FindObjectOfType<ButtonOld>().messagee = gameObject;
        button = (Button)FindObjectOfType<Button>();
        button.onClick.AddListener(ButtonPressed);
        
    }

    public void OnDestroy()
    {button.onClick.RemoveAllListeners();
    }

    public void ButtonPressed()
    {
        _weapon.Shoot(9);
    
    }

    private void Update ()
    {
    //    if (Input.GetButtonDown("Fire1"))
      //      _weapon.Shoot(9);
    }
}