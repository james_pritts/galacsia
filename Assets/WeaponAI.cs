﻿using UnityEngine;
using System.Collections;

public class WeaponAI : MonoBehaviour
{
    public float shotTimer;
    public float shotDelay = 1.0f;
    public Weapon weapon;
    public bool isFiriing = false;
	// Use this for initialization
	void Start ()
	{
	    shotTimer = Time.time;
	    weapon = GetComponent<Weapon>();
	}
	  
	// Update is called once per frame
	void Update () {
	    if (Time.time>shotTimer+shotDelay&&isFiriing)
	    {
	        shotTimer = Time.time;
            weapon.Shoot(8); 

	    }
	}
}
