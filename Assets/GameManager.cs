﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

/// <summary>
/// The GameManager class is in charge of controling the game state.
/// This class takes care of dying, scoring, and respawning.
/// </summary>
public class GameManager : MonoBehaviour
{
    
    public GameObject playerResource;   // Saved prefab of player object
    public GameObject playerInstance;   // The instantiated instance of said object
    
    public List<GameObject> enemys;     // List container of all the enemies
    
    public AudioClip stageClear;        // Audio clip to play when stage has been cleared
    public AudioClip died;
    public AudioClip NewHighScoreClip;
    public AudioSource victorySound;
    
    public bool levelEnded = false;
    public bool playerRespawning = false;

    public int lives = 3;               // Number of Player lives remaining
    public int Score = 0;               // Number of point accululated during game
    public int HighScore=1000;          // Current Highscore
    public int nextScoreForCheer;       // The score that's hit before everyone cheers
    
    public Text ScoreText;
    public Text HighscoreText;
    public Text LivesText;

    public bool isClosing = false;

    public float lossTimer = 0f;
    public bool lostLives = false;
  
    public void Start() 
    {
        playerResource = Resources.Load<GameObject>("Player");
        playerInstance = (GameObject)Instantiate(playerResource);
        GenerateEnemyField();
    }
    /// <summary>
    /// This plops out a 6 / 6 grid of sibling game objects in a rainbow display
    /// </summary>
    private void GenerateEnemyField()
    {
        for (int y = 0; y < 5; y++)
        {
            for (int x = -5; x < 5; x++)
            {
                Color leftColor = Color.Lerp(Color.red, Color.yellow, (x +5f)/ 10f);
                Color rightColor = Color.Lerp(Color.white, Color.yellow, (x+5f ) / 10f);
                  Color middleColor = Color.Lerp(Rainbow(y /5f),rightColor,0.5f);
                 GameObject enemySpawn =
                    (GameObject)
                        Instantiate(Resources.Load<GameObject>("EnemyShip"), transform.position + Vector3.left*2,
                            transform.rotation);
                enemySpawn.transform.position = transform.position + (Vector3.left*x*2) + (Vector3.up*y*2);
                enemySpawn.layer = 8;
                enemySpawn.GetComponent<Renderer>().material.color = middleColor;
            enemys.Add(enemySpawn);
            }
        }
    }
    //Todo: refactor into untility class


/// <summary>
/// The Rainbow function is given a float f and returns a color of the rainbow.
/// 0 = Red, 0.5 = Green, 1 = Blue
/// </summary>
/// <param name="f"></param>
/// <returns></returns>
    public Color Rainbow(float f)
    {
        if (f < 0.5f)
            return Color.Lerp(Color.red, Color.green, f*2f);
        else
            return Color.Lerp(Color.green, Color.blue, (f - 0.5f)*2f);

        }
    /// <summary>
    /// Winning is a coroutine that is started when the stage is cleared
    /// It plays a song then restartes the battle field.
    /// </summary>
    /// <returns></returns>
    public IEnumerator winning()
    {
        victorySound = gameObject.AddComponent<AudioSource>();
        victorySound.clip = stageClear;
        victorySound.loop = false;
        victorySound.Play();
        
        yield return new WaitForSeconds(2f);
        
        GenerateEnemyField();
        levelEnded = false;

    }
    public IEnumerator respawning()
    {
        if (lossTimer == 0)
        {
            yield return new WaitForSeconds(2f);
            playerInstance = (GameObject) Instantiate(playerResource);
            playerRespawning = false;
        }
        yield return null;

    }
    /// <summary>
    /// This sets a flag to let the program know it's closing
    /// </summary>
    public void OnApplicationQuit()
    {
        isClosing = true;
    }
     /// <summary>
     /// Adds scoreAmount to Score
     /// </summary>
     /// <param name="scoreAmount">Number to add to score</param>
     /// 
    public void ScoreAdd(int scoreAmount)
    {
        if (isClosing==true)
            return;
        Score += scoreAmount;
        if(ScoreText!=null)
            { ScoreText.text = ""+Score;}
        if (HighScore < Score)
        {
            HighScore = Score;
            HighscoreText.text = HighScore.ToString();
            if (Score > nextScoreForCheer)
            {
                GetComponent<AudioSource>().PlayOneShot(NewHighScoreClip);
                nextScoreForCheer = HighScore * 2;
            }
        }
        }
       /// <summary>
       /// Substracts a life from total, updates UI, and starts losing coroutine
       /// </summary>
    public void RemoveLife()
    {
        if (LivesText==null)
            return; 
        lives -= 1;
        LivesText.text = lives.ToString();
        if (lives == 0)
            StartCoroutine(Losing());
    }

   public IEnumerator Losing()
    {
        lostLives = true;
        foreach (AudioSource adSource in FindObjectsOfType<AudioSource>())
            adSource.volume = 0.01f;

        GetComponent<AudioSource>().clip = died;
        GetComponent<AudioSource>().volume = 1f;
        GetComponent<AudioSource>().Play();
        yield return null;
        while (lossTimer<3f)
        {
            lossTimer += Time.deltaTime;
            yield return null;
        }
        Application.LoadLevel(0);
        yield return null;
    }

    // Update is called once per frame
	void Update () {
	    if (playerInstance == null&&playerRespawning==false&&lostLives==false)
	    {
	        playerRespawning = true;
	        StartCoroutine(respawning());
	    }
	    if (enemys.Count == 0 &&levelEnded==false)
	    {
	        levelEnded = true;
	        StartCoroutine(winning());
	    }

        
    }
}
