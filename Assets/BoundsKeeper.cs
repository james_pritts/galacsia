﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoundsKeeper : MonoBehaviour {

	public List<Transform> transToKeep;

    public Vector2 bounds;

	//public float threshold = 128f;
    
	public bool isSquare;
	// Use this for initialization 
	void Start () {
	
	}
	void FixedUpdate () {

		Object[] objects = FindObjectsOfType(typeof(Collider));
        
        foreach(Object o in objects)
		{
			Collider t = (Collider)o;
			if (
				//t.parent == null&&
				transform!=t.transform)
			    
			{//Debug.Break ();
				//if(isSquare)
					InvertPosition(t.transform);
			
				//else 
				//	InvertPosition(t);
				//InvertPosition(t);
			}
		}

	}

	void SetBacktoEdge(Transform t)

	{
		if(Mathf.Abs(t.position.x)>bounds.x||
		   Mathf.Abs(t.position.z)>bounds.y)
		{Debug.Break();
			t.Translate(-t.forward*t.position.magnitude	*2);//rigidbody.velocity.normalized*-t.position.magnitude*2);
		}
	}

	void InvertPosition(Transform t)
	{

		if(  t.position.x  > bounds.x  + transform.position.x||//if the abosolute value of the position is beyond the bounds
            t.position.x < -bounds.x - transform.position.x ||
            t.position.y > bounds.y + transform.position.y||
            t.position.y < -bounds.y - transform.position.y
            )
			{
			t.position=-t.position*0.9f;
	//t.position=new Vector3(Mathf.Repeat(

		//	if(t.rigidbody!=null&&t.rigidbody.velocity.magnitude<0.1f)
		//		t.rigidbody.AddForce(new Vector3(Random.Range(2f,3f),0f,Random.Range (2f,3f)),ForceMode.VelocityChange);
		
		}
	
	}
	// Update is called once per frame
	void WrapTransform (Transform t) {

		Vector3 tempPosition= t.position;

		if (transform.position.x + t.position.x>bounds.x)
		{	
			tempPosition.x-=bounds.x*2;
			tempPosition.z=-t.position.z;
		}	
		if (transform.position.x + t.position.x<-bounds.x)
		{	tempPosition.x=bounds.x*2;
			tempPosition.z=-t.position.z;

		}
		if (transform.position.z + t.position.z>bounds.y)
		{	tempPosition.z=-bounds.y*2;
			tempPosition.x=-t.position.x;

		}
		if (transform.position.z + t.position.z<-bounds.y)
		{		tempPosition.z=bounds.y*2;
				tempPosition.x=-t.position.x;

		}
				t.position=tempPosition;
	}

}
