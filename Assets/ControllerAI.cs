﻿//using System;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ControllerAI : ControllerInput {

	// Use this for initialization
    public Vector3 homePosition;
    public float currentAngle = 0f;
    public float spinRate = 5.0f;
    public float strafe = 5f;
    public bool strafingLeft = false;
    public bool imOut = false;
    public WeaponAI weaponAI;
  
    public void Start()
    {
        controller = GetComponent<ShipController>();
        weaponAI = GetComponent<WeaponAI>();
        homePosition = transform.position;
        manager = GameObject.FindObjectOfType<GameManager>();
    }

    public void  OnDestroy()
    {
        manager.enemys.Remove(gameObject);
       if (isClosing==false )
        manager.ScoreAdd(15);
    }

    public bool isClosing = false;
    public void OnApplicationQuit()
    {
        isClosing = true;
    }
    // Update is called once per frame
    public void FixedUpdate()
    {
        if (imOut == false)
        {
            Strafe();

            if (Random.Range(0, 10000) == 0)
                imOut = true;
        }
        else

        {
            Serpentine();
    }

}

    public void Serpentine()
    {
        if(weaponAI!=null)
        weaponAI.isFiriing = true;
        controller.Move( new Vector2(Mathf.Cos(transform.position.y/2),1));
    }
    public void Strafe()
    {
        weaponAI.isFiriing = false;
        if (strafingLeft)
        {controller.Move(-Vector2.right);
            if (transform.position.x>homePosition.x + strafe)
                strafingLeft = false;
        } 
        else
        {
            controller.Move(Vector2.right);
            if (transform.position.x<homePosition.x  -strafe)
                strafingLeft = true;
            
        }
       
    }

    void Spin ()
	{
	    currentAngle+=spinRate;
	    if (currentAngle > 360)
	        currentAngle -= 360;
        controller.Move(new Vector2(Mathf.Cos(currentAngle * Mathf.Deg2Rad), Mathf.Sin(currentAngle * Mathf.Deg2Rad)));
	}
}
