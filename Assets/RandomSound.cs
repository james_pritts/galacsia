﻿using UnityEngine;
using System.Collections;

public class RandomSound : MonoBehaviour
{
    public string Folder;
	// Use this for initialization
	public AudioClip Randomize () {
        AudioClip[] clips = Resources.LoadAll<AudioClip>("Explosions");
        return (clips[Random.Range(0, clips.Length - 1)]);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
