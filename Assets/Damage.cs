﻿using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour
{

    public int maxHits = 1;
    public int numberOfHits = 0;
    public AudioClip boomSound;
    public string hurtBy;
	// Use this for initialization
	void Start () {
	
	}

    public void OnCollisionEnter(Collision c)
    {
        if(c.transform.tag == tag)
            numberOfHits++;
        if (c.gameObject.tag == "Player")
            Destroy(c.gameObject);
    
        
    }

    // Update is called once per frame
	void Update () {
	    if ((numberOfHits >= maxHits)&&(isDead==false))
	    {
	        isDead = true;
	        Instantiate(Resources.Load("Explode"),transform.position,transform.rotation);
            bool didWeapon = (Random.Range(0f, 1f)>0.8f);
             if (didWeapon ==true) 
            {
                Debug.Log(gameObject);
                Debug.Log(didWeapon);
         
                GameObject powerUp = (GameObject)Instantiate(Resources.Load("PowerUp"));
                powerUp.transform.position = transform.position;
            }
           StartCoroutine( Die());
	    }
	}

    public bool isDead = false;
    public IEnumerator Die()
    {
       
        GetComponent<AudioSource>().clip = gameObject.GetComponent<RandomSound>().Randomize();
        GetComponent<AudioSource>().Play();
        GetComponent<AudioSource>().loop = true;
      //  gameObject.AddComponent<Rigidbody>();
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<ShipController>().enabled = false;

        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().AddForce(Random.insideUnitSphere*20f,ForceMode.VelocityChange);
        GetComponent<Rigidbody>().AddTorque(Random.insideUnitSphere*10f,ForceMode.VelocityChange);
        float deadTimer = 0f;
        while (deadTimer < 1.5f)
        {
            deadTimer += Time.deltaTime;
            yield return null;
        }
        
        Destroy(gameObject);
        yield return null;
    }
    
}
